// raaSimpleLinkedList.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <stdio.h>
#include <conio.h>

struct raaElement
{
	raaElement *m_pNext;
	raaElement *m_pLast;
	int iNumber;
};

raaElement *g_pHead=0;
raaElement *g_pTail=0;

void printList();
void pushHead(raaElement* raaElement);
void pushTail(raaElement* raaElement);

raaElement* createNewElement(int iNumber);
raaElement* deleteElement(raaElement *pElement);
raaElement* popHead();
raaElement* popTail();

void remove(raaElement *pElement);

int _tmain(int argc, _TCHAR* argv[])
{
	raaElement *pElement=0;

	raaElement* pElementSpecial;

	pElementSpecial= createNewElement(1);
	pushHead(pElementSpecial);

	pElement = createNewElement(2);
	pushTail(pElement);

	pElement=createNewElement(3);
	pushHead(pElement);

	pElement=createNewElement(4);
	pushTail(pElement);

	pElement = createNewElement(5);
	pushTail(pElement);


	printList();

	delete popHead();
	delete popTail();

	pushHead(popTail());

	printf("\n\n\n");
	printList();

	deleteElement(pElementSpecial);

	printf("\n\n\n");
	printList();

	_getch();
	return 0;
}

void printList()
{
	unsigned int uiCount=0;
	for(raaElement *pElement=g_pHead;pElement;pElement=pElement->m_pNext)
	{
		printf("Element: (data = %d) %d\n", pElement->iNumber, 
			uiCount++);
		printf("\tNode\n");
	}
}

void pushHead(raaElement* raaElement) {
	if (!(raaElement->m_pLast == 0 && raaElement->m_pNext == 0)) {
		printf("raaElement not initialised properly, memory addresses unsafe, will not push head\n");
		return;
	}

	if ((g_pHead == 0 && g_pTail == 0)) {
		printf("raaElement is first element in list\n");
		g_pHead = raaElement;
		g_pTail = raaElement;
		return;
	}

	printf("raaElement will be appended to head of list\n");

	raaElement->m_pNext = g_pHead;
	g_pHead->m_pLast = raaElement;
	g_pHead = raaElement;
	return;
}

void pushTail(raaElement* raaElement) {
	if (!(raaElement->m_pLast == 0 && raaElement->m_pNext == 0)) {
		printf("raaElement not initialised properly, memory addresses unsafe, will not push tail\n");
		return;
	}

	if ((g_pHead == 0 && g_pTail == 0)) {
		printf("raaElement is first element in list\n");
		g_pHead = raaElement;
		g_pTail = raaElement;
		return;
	}

	printf("raaElement will be appended to tail of list\n");

	raaElement->m_pLast = g_pTail;
	g_pTail->m_pNext = raaElement;
	g_pTail = raaElement;
	return;
}

void clearMemoryAddressForRaaElement(raaElement* raaElementToRemove) {
	raaElementToRemove->m_pLast = 0;
	raaElementToRemove->m_pNext = 0;
	return;
}

void clearMemoryAddressAfterLastElementRemoved() {
	g_pHead = 0;
	g_pTail = 0;
}

raaElement* popHead() {
	if ((g_pHead == 0 && g_pTail == 0)) {
		printf("No elements in list to pop\n"); 
		return 0;
	}

	raaElement* raaElementToRemove;
	if (g_pHead == g_pTail) {
		printf("head and tail are the same, removing only element in list");
		raaElementToRemove = g_pHead;
		clearMemoryAddressAfterLastElementRemoved();
	} else {
		printf("popping head of list\n");
		raaElementToRemove = g_pHead;
		g_pHead = g_pHead->m_pNext;
		g_pHead->m_pLast = 0;
	}

	clearMemoryAddressForRaaElement(raaElementToRemove);
	return raaElementToRemove;
}

raaElement* popTail() {
	if ((g_pHead == 0 && g_pTail == 0)) {
		printf("No elements in list to pop\n");
		return 0;
	}

	raaElement* raaElementToRemove;
	if (g_pHead == g_pTail) {
		printf("head and tail are the same, removing only element in list");
		raaElementToRemove = g_pTail;
		clearMemoryAddressAfterLastElementRemoved();
	} else {
		printf("popping tail of list\n");
		raaElementToRemove = g_pTail;
		g_pTail = g_pTail->m_pLast;
		g_pTail->m_pNext = 0;
	}

	clearMemoryAddressForRaaElement(raaElementToRemove);
	return raaElementToRemove;
}

raaElement* createNewElement(int iNumber)
{
	raaElement *pElement=new raaElement;

	pElement->m_pLast=0;
	pElement->m_pNext=0;
	pElement->iNumber = iNumber;

	// initialise data

	return pElement;
}

raaElement* deleteElement(raaElement *pElement)
{
	raaElement *pE=pElement;

	if(pE)
	{
		// remove from list
		remove(pE);

		// may also want to clean up data
		delete pE;
		pE=0;
	}

	return pE;
}

void remove(raaElement *pElementToRemove) {
	for (raaElement *pElement = g_pHead; pElement; pElement = pElement->m_pNext)
	{
		if (pElement == pElementToRemove) {
			printf("Removing element - (data = %d)\n", pElement->iNumber);
			if (pElement == g_pHead) {
				popHead();
				break;
			}

			if (pElement == g_pTail) {
				popTail();
				break;
			}

			raaElement* pElementBeforeRemovalElement = pElement->m_pLast;
			raaElement* pElementAfterRemovalElement = pElement->m_pNext;
			pElementBeforeRemovalElement->m_pNext = pElementAfterRemovalElement;
			pElementAfterRemovalElement->m_pLast = pElementBeforeRemovalElement;

			pElement->m_pLast = 0;
			pElement->m_pNext = 0;
			break;
		}

	}
}
